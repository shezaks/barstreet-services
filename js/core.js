$(document).on("ready", function () {
	main.init();
	var url = window.top.location.pathname;
	if (url == '/coctails') CheckAnchor('/coctails');
});

$(window).on("load", function () {
	main.onload();
}).on("resize", function () {
	main.resize();
});

var main = {
	init: function () {
		if ($('.count-info').length != 0) {
			var targets = $('[rel~=tooltip]'),
				target = false,
				tooltip = false,
				title = false;

			targets.bind('mouseenter', function () {
				target = $(this);
				tip = target.attr('title');
				tooltip = $('<div id="tooltip"></div>');

				if (!tip || tip == '')
					return false;

				target.removeAttr('title');
				tooltip.css('opacity', 0)
					.html(tip)
					.appendTo('body');

				var init_tooltip = function () {
					if ($(window).width() < tooltip.outerWidth() * 1.5)
						tooltip.css('max-width', $(window).width() / 2);
					else
						tooltip.css('max-width', 340);

					var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
						pos_top = target.offset().top - tooltip.outerHeight() - 20;

					if (pos_left < 0) {
						pos_left = target.offset().left + target.outerWidth() / 2 - 20;
						tooltip.addClass('left');
					}
					else
						tooltip.removeClass('left');

					if (pos_left + tooltip.outerWidth() > $(window).width()) {
						pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
						tooltip.addClass('right');
					}
					else
						tooltip.removeClass('right');

					if (pos_top < 0) {
						var pos_top = target.offset().top + target.outerHeight();
						tooltip.addClass('top');
					}
					else
						tooltip.removeClass('top');

					tooltip.css({left: pos_left, top: pos_top})
						.animate({top: '+=10', opacity: 1}, 50);
				};

				init_tooltip();
				$(window).resize(init_tooltip);

				var remove_tooltip = function () {
					tooltip.animate({top: '-=10', opacity: 0}, 50, function () {
						$(this).remove();
					});

					target.attr('title', tip);
				};

				target.bind('mouseleave', remove_tooltip);
				tooltip.bind('click', remove_tooltip);
			});

		}
		$('.minus').click(function () {
			var $input = $(this).parent().find('input');
			var count = parseInt($input.val()) - 25;
			count = count < 25 ? 25 : count;
			$input.val(count);
			$input.change();
			return false;
		});
		$('.plus').click(function () {
			var $input = $(this).parent().find('input');
			$input.val(parseInt($input.val()) + 25);
			$input.change();
			return false;
		});
		$(".comment-link").on("click", function () {
			$(".comment-textarea").toggleClass('_hidden');
		});
		$('.item ul').each(function () {

			if (Modernizr.mq('(min-width: 1030px)')) {
				var height = $(this).closest(".item").is(".big") ? 165 : 200;
				if ($(this).height() > height) {
					$(this).height(height);
					if ($(this).parents('.text').length > 0)
						$(this).parents('.text').find('.show-list').show();
					else {
						$(this).parents('.item').find('.show-list').show();
					}
				}
			} else {
				if ($(this).parents('.text').length > 0) {
					$(this).parents('.text').addClass('autoheight');
					$(this).parents('.text').find('.show-list').hide();
				} else {
					$(this).parents('.item').find('ul').addClass('autoheight');
					$(this).parents('.item').find('.show-list').hide();
				}
				$(this).addClass('autoheight');

			}
		});
		$('.show-list').on("click", function () {
			if (!$(this).hasClass("active")) {
				if ($(this).parents('.text').length > 0) {
					$(this).parents('.text').addClass('autoheight');
					$(this).parents('.text').find('ul').addClass('autoheight');
				} else {
					$(this).parents('.item').find('ul').addClass('autoheight');
				}
				$(this).text('Скрыть x');
				$(this).addClass('active');
			} else {
				if ($(this).parents('.text').length > 0) {
					$(this).parents('.text').removeClass('autoheight').find(".quote p").removeClass('nonemax');
					$(this).parents('.text').find('ul').removeClass('autoheight');
				} else {
					$(this).parents('.item').find('ul').removeClass('autoheight');

				}
				$(this).text('Показать все >>');
				$(this).removeClass('active');
			}
			return false;
		});
		$(".showall").on("click", function () {
			if (!$(this).hasClass("active")) {
				$(this).parents('li').addClass('autoheight').find(".quote p").addClass('nonemax');
				$(this).parents('.bx-viewport').addClass('autoheight');
				$(this).text('Скрыть');
				$(this).addClass('active');
			} else {
				$(this).parents('li').removeClass('autoheight').find(".quote p").removeClass('nonemax');
				$(this).parents('.bx-viewport').removeClass('autoheight');
				$(this).text('Показать полностью');
				$(this).removeClass('active');
			}
			return false;
		});
		$(".phone-number_checker a").on("click", function () {
			if (!$(this).hasClass("active")) {
				$(this).parent().find("a.active").removeClass("active");
				$(this).addClass("active");
				$(this).parent().prev().html($(this).attr("data-number"));
			}
			return false;
		});
		$(".head-menu_triger").on("click", function () {
			if ($(this).hasClass("active")) {
				$(this).removeClass("active");
				$(this).next().removeClass("active");
				$(".head-menu_triger").next().attr("style", "");
			} else {
				$(this).addClass("active");
				var h = $(window).outerHeight() - $(this).closest(".block-head").outerHeight() - 68;
				var c = $(this).next().outerHeight();
				if (h > c) {
					$(this).next().height(h);
				}
				$(this).next().addClass("active");
			}
			return false;
		});
		$(".target_outside-link").on("click", function () {
			window.location = $(this).closest('article').attr('data-href');
			return false;
		});
		$(".target_inside-link").on("click", function () {
			window.location = $(this).find("img").attr('data-href');
			return false;
		});
		$(".target-link").on("click", function () {
			window.location = $(this).attr('data-href');
			return false;
		});
		$(".head-nav nav").on("click", function () {
			return false;
		});
		$(".head-nav nav a").on("click", function () {
			window.location = $(this).attr("href");
		});
		$("body").on("click", function () {
			if ($(".head-menu_triger").hasClass("active")) {
				$(".head-menu_triger").removeClass("active");
				$(".head-menu_triger").next().removeClass("active");
				$(".head-menu_triger").next().attr("style", "");
			}
		}).on("click", ".btn-more", function () {
			var el = $(this).parent().prev();
			if (el.hasClass("open")) {
				el.removeClass("open");
				$(this).html("Читать полностью").attr("title", "Читать полностью").removeClass("open");
			} else {
				el.addClass("open");
				$(this).html("Свернуть").attr("title", "Свернуть").addClass("open");
			}
		})/*.on("click", ".coctail_read-more", function(){
		 if(!$(this).hasClass("open")){
		 $(this).addClass("open").text("Свернуть");
		 $(this).closest(".text").find($(this).attr("data-element")).slideDown("slow");
		 } else {
		 $(this).removeClass("open").text("Способ приготовления");
		 $(this).closest(".text").find($(this).attr("data-element")).slideUp("slow");
		 }
		 return false;
		 })*/.on("click", ".coctails_catalog-item .btn", function () {
			if (!$(this).hasClass("added")) {
				$(this).parent('coctails_catalog-item').find('div.image img');
				$(this).addClass("added").text("ЗАКАЗАНО");
				$(".coctails_cart i").each(function () {
					var count = parseFloat($(this).text().replace(' коктейлей', '')) + 1;
					$(this).text("" + count + " " + main.coctails.declens(count, ['коктейль', 'коктейля', 'коктейлей']));
				})
				var id = $(this).data('id');
				flyInCart(id);
				console.log(id);
				if ((typeof id !== "undefined") && (id != '')) {
					saveInCookie('basket', id, 25);
				}
			}
			return false;
		}).on("click", ".cart-item_remove", function () {
			if (confirm("Вы хотите удалить этот коктейль?")) {
				$(this).closest(".item-row").remove();
				main.coctails.calc();
				deleteFromCookie('basket', $(this).data('id'));
			}
			return false;
		}).on("input change paste", ".block_coctails-cart input[name='count']", function () {
			var cnt = $(this).val().replace(/[^\d]/g, '');
			//$(this).val($(this).val().replace(/[^\d]/g,''));
			$(this).val(cnt);
			main.coctails.calc();
			var id = $(this).data('id');
			if ((typeof id !== "undefined") && (id != '')) {
				saveInCookie('basket', id, $(this).val())
			}
		}).on("focusin", ".block_coctails-cart input[name='count']", function () {
			main.init.input_old = parseFloat($(this).val());
		}).on("focusout", ".block_coctails-cart input[name='count']", function () {
			if ($(this).val() < 25) {
				$(this).val(25);
				saveInCookie('basket', $(this).data('id'), 25);
				main.coctails.calc();
			} else if ($(this).val().length === 0) {
				$(this).val(0);
				main.coctails.calc();
			}

		});
		$('.coctail_read-more').bind("click", function () {
			if (!$(this).hasClass("open")) {
				$(this).addClass("open").text("Свернуть");
				$(this).closest(".text").find($(this).attr("data-element")).slideDown("slow");
			} else {
				$(this).removeClass("open").text("Способ приготовления");
				$(this).closest(".text").find($(this).attr("data-element")).slideUp("slow");
			}
			return false;
		});
		$('select').selectbox();
		$(".service-coctails_filter").on("change", "select, input", function () {
			var params = {}, url = '';
			$(this).closest(".service-coctails_filter").find("select").each(function () {
				var key = $(this).attr("name");
				params[key] = $(this).find("option:checked").attr("value");
				//if (key == 'bars') url = $(this).find("option:checked").attr("data-url");
				if (key == 'bars') url = $(this).find("option:checked").attr("data-url");
			});

			$(this).closest(".service-coctails_filter").find("input").each(function () {
				if ($(this).is(":checked")) {
					var key = $(this).attr("name");
					params[key] = $(this).attr("value");
					if (key == 'bars') url = $(this).attr("data-url");
				}
			});

			$.ajax({
				url: '/coctails',
				type: 'post',
				data: {
					ajax: 'Y',
					alco: params['alco'],
					bars: params['bars'],
					ingredients: params['ingredients']
				},
				success: function (data) {
					if (data) {
						$('section.block-cont.g-wrap').html(data);
						main.init();
						if (!url) url = 'all';
						window.top.location.hash = url;
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {

				}
			});
		});
		main.coctails.calc();
		$('.main_slider-top').bxSlider({
			mode: 'fade',
			captions: true
		});
		$('.service-block_slider ul').bxSlider();
		$('.event_slider-wrap ul').bxSlider({
			mode: 'fade'
		});
		$('.team-slider_wrap ul').bxSlider({
			mode: 'fade',
			pagerCustom: '.slider-thumb'
		});
		$(".main_clients-carousel").bxSlider({
			minSlides: 3,
			maxSlides: 5,
			slideWidth: 130,
			slideMargin: 46,
			auto: true,
			autoHover: true,
			pause: 7000
		});
		$(".services-coctails_slider ul").bxSlider({
			minSlides: 1,
			maxSlides: 4,
			slideWidth: 220,
			slideMargin: 20,
			auto: true,
			autoHover: true,
			pause: 7000
		});
		$("body").on("input change paste keyup mouseup", "input.error", function () {
			if ($(this).hasClass('subscr-email')) {
				$(this).removeClass("error");
			} else {
				if ($(this).val() !== "" && !/^ *$/.test($(this).val())) {
					$(this).removeClass("error");
					$(this).next().remove();
				}
			}
		});
		if ($(".date-picker").length > 0) {
			$(".date-picker").datepicker({
				firstDay: 1,
				dateFormat: "d MM yy",
				minDate: 0,
				showAnim: "fadeIn"
			}).datepicker("setDate", new Date());
		}
		$("body").on("input change paste keyup mouseup", "input.number_separ", function () {
			var val = $(this).val();
			var prenumber = val.split(" ").join("");
			var number = parseFloat(prenumber);
			if (!isNaN(number)) {
				val = prenumber.replace(/\B(?=(\d{3})+(?!\d))/g, " ");
			}
			$(this).val(val);
		});
		$("body").on("change", "input[type='file']", function () {
			var fileAPI = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;
			var fileName;
			if (fileAPI && $(this)[0].files[0]) {
				fileName = $(this)[0].files[0].name;
			} else {
				fileName = $(this).val().replace("C:\\fakepath\\", "");
			}
			if (!fileName.length) {
				return;
			} else {
				$(this).parent().find(".filename").html(fileName);
			}
		});
		$('.faq-list__li').on("click", function () {
			$('.faq-list__li').removeClass('_active');
			var li = $(this);
			li.addClass('_active');
			var href = li.find('.faq-list__a').attr('href');
			$('.faq-tab').addClass('_hidden');
			$(href).removeClass('_hidden');
		});
		$('.faq-list__a').on("click", function (e) {
			e.preventDefault();
		});
		$('.faq-left-link').on("click", function (e) {
			e.preventDefault();
			yaCounter10660447.reachGoal('ASKPUSH');
			main.form.open(this, 'manager', {'title': 'Задать вопрос', 'desc': 'Наш менеджер свяжется с вами<br> в ближайшее время'});
		});

		$('.head-contacts-callback__link').on("click", function (e) {
			e.preventDefault();
			main.form.open(this, 'callback', {'title': 'Обратный звонок', 'desc': 'Наш менеджер свяжется с вами<br> в ближайшее время', 'ga': 'callback'});
		});
		$('.scroll-to').on('click', function () {
			var el = $(this).attr('href');
			$("html,body").animate({scrollTop: $(el).offset().top}, 700);
		});

		//var url = window.top.location.pathname;
		//if (url == '/coctails') CheckAnchor('/coctails');
	},
	onload: function () {
		main.footer();
	},
	resize: function () {
		main.footer();
		if ($(".date-picker").length > 0) {
			$(".date-picker").datepicker("hide");
		}
		if ($(document).outerWidth() > 632) {
			if ($(".head-menu_triger").hasClass("active")) {
				$(".head-menu_triger").removeClass("active");
				$(".head-menu_triger").next().removeClass("active");
				$(".head-menu_triger").next().attr("style", "");
			}
		}
		$('.services-block_other .item.small ul').each(function () {

			if (Modernizr.mq('(min-width: 1030px)')) {
				if ($(this).height() > 200) {
					$(this).removeClass('autoheight');
					$(this).parents('.text').removeClass('autoheight');
					$(this).parents('.text').find('.show-list').show();

				}
			} else {
				$(this).parents('.text').addClass('autoheight');
				$(this).addClass('autoheight');
				$(this).parents('.text').find('.show-list').hide();
			}
		});
	},
	footer: function () {
		// $(".block-cont").css({"padding-bottom": $(".block-foot").height()});
	},
	coctails: {
		calc: function () {
			if ($(".block_coctails-cart").length > 0) {
				var total = 0, count = 0, delivery = 0;
				$(".block_coctails-cart").find(".item-row").each(function () {
					var item_count = parseFloat($(this).find("input[name='count']").val());
					if (isNaN(item_count)) {
						item_count = 0;
					}
					var t = parseFloat($(this).find(".price i").text()) * item_count;
					total += t;
					count += item_count;

					$(this).find(".calc i").html(t.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
				});
				$(".cart-total").find(".count").html(count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " " + main.coctails.declens(count, ['коктейль', 'коктейля', 'коктейлей']));
				$(".cart-total").find(".price").html(total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
				delivery = parseInt($('#delivery').html().replace(/\s+/g, ''));

				$('#itog_sum').html((total + delivery).toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
				if (total + delivery >= 35000) {
					$(".cart-action").find(".order").removeClass("disabled").removeAttr("onclick");
					$('.cart-itog .warning').hide();
				} else {
					$(".cart-action").find(".order").addClass("disabled").attr("onclick", "return false;");
					$('.cart-itog .warning').show();
				}
			}
		},
		declens: function (number, titles) {
			cases = [2, 0, 1, 1, 1, 2];
			return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
		}
	},
	map: {
		init: function () {
			$("body").find(".map").each(function () {
				var data = $(this).attr("data-position").split(",");
				var latlng = new google.maps.LatLng(parseFloat(data[0]), parseFloat(data[1]));
				var settings = {
					zoom: 16,
					scrollwheel: false,
					disableDefaultUI: true,
					center: latlng
				};

				var map = new google.maps.Map(this, settings);

				var companyPos = new google.maps.LatLng(parseFloat(data[0]), parseFloat(data[1]));
				var image = 'img/marker.svg';
				var companyMarker = new google.maps.Marker({
					position: companyPos,
					map: map,
					icon: image,
					zIndex: 3
				});
			});
		}
	},
	form: {
		open: function (el, type, text, code) {
			var content;
			var gap = text && 'ga' in text ? text['ga'] : '';
			var ret = "return main.form.submit(this, '" + type + "', '" + code + "', '" + gap + "');";
			if ((type === "manager") || (type === "service") || (type === "price") || (type === "callback")) {
				content = '<div class="g-form">\
				<p class="h4">' + text['title'] + '</p>';
				if (type === "service") {
					content += '<p class="desc-one">Отправьте заявку и получите подробную консультацию об услуге. <i class="pink">Консультация бесплатна!</i></p>';
				}
				content += '<form action="/message" method="post" onsubmit="' + ret + '" data-theme="Консультация менеджера">\
                 <input type="hidden" name="Domain" id="Domain">\
                <input type="hidden" name="Keyword" id="Keyword" >\
                <input type="hidden" name="Send" value="1" />';
				if (type === "price") {
					content += '<input type="hidden" name="path2Module" value="/thanx1">';
				} else {
					content += '<input type="hidden" name="path2Module" value="/message">';
				}

				content += '<input type="hidden" name="FormType" value="' + type + '" />\
                <input type="hidden" name="Title" value="' + text['title'] + '" />\
				<div class="cols_wrap clearfix">\
					<div class="col_left form_title">Ваше имя:</div>\
					<div class="col_right form_line"><input type="text" name="name" data-required="true" /></div>\
				</div>\
				<div class="cols_wrap clearfix">\
					<div class="col_left form_title">Телефон:</div>\
					<div class="col_right form_line"><input type="tel" name="phone" data-required="true" /></div>\
				</div>';
				if (type !== "callback" && type !== "service") {
					content += '<div class="cols_wrap clearfix">\
					<div class="col_left form_title">Почта:</div>\
					<div class="col_right form_line"><input type="email" name="email" data-required="true" /></div>\
				</div>';
				}
				if (type !== "price" && type !== "callback") {
					content += '<div class="cols_wrap clearfix last">\
					<div class="col_left form_title"><a href="#comment" class="comment-link">Комментарий</a></div>\
					<div class="col_right form_line"><textarea name="comment" rows="7" class="comment-textarea"></textarea></div>\
				</div>';
				}
				content += '<div class="cols_wrap clearfix">\
					<div class="clearfix">\
						<div class="form_submit"><input type="submit" class="btn" value="Отправить" /></div>\
						<div class="form_submit-desc">' + text['desc'] + '</div>\
					</div>\
				</div>\
				</form>\
				</div>';
			} else if (type === "alert") {
				content = '<div class="g-form">\
				<p class="h4">' + text[0] + '</p>\
				<p>' + text[1] + '</p>\
				</div>';
			} else if (type === "howitworks") {
				content = '<div class="g-form hiw">\
				<p class="h4">Доставка коктейлей на любое мероприятие</p>\
                <form action="/message" method="post" data-theme="Доставка коктейлей на любое мероприятие">\
				<div class="cols_wrap clearfix">\
					<div class="col_left form_title text-center"><img src="/img/how1.png"></div>\
					<div class="col_right form_line"><p class="h5">Выберите коктейли</p><p>Минимальная сумма заказа коктейлей составляет 35 000 рублей</p></div>\
				</div>\
				<div class="cols_wrap clearfix">\
					<div class="col_left form_title text-center"><img src="/img/how2.png"></div>\
					<div class="col_right form_line"><p class="h5">Согласуем детали и стоимость</p><p>Наш менеджер позвонит Вам и уточнит детали заказа, по возможности \
					откорректирует коктейли.</p></div>\
				</div>\
				<div class="cols_wrap clearfix">\
					<div class="col_left form_title text-center"><img src="/img/how3.png"></div>\
					<div class="col_right form_line"><p class="h5">Наслаждайтесь коктейлями</p><p>Мы привезем все необходимое оборудование и ингредиенты и сделаем вкуснейшие коктейли.</p></div>\
				</div>\
				<div class="cols_wrap m30 clearfix"><div class="grey-sep"></div></div>\
				<div class="cols_wrap clearfix">\
				<div class="text-center">\
					<h6>Остались вопросы? Звоните <span>+7 (495) 374-61-62</span></h6>\
					</div>\
					</div>\
				</div>\
				</div>\
				</form>\
				</div>';
			}
			$("body").append("<div id='pop-up'>\
				<div class='pop-up-body " + type + "'>\
				<i class='close' title='Закрыть' onclick='main.form.close();'></i>\
				<div class='pop-up-content " + type + "'>" + content + "</div></div>\
				<div class='pop-up-over' onclick='main.form.close();'></div>\
				</div>");
			if (type === "call" || type === "alert") {
				main.form.resize();
			}
			$("body").addClass("pop-up");
			$(".comment-link").on("click", function () {
				$(".comment-textarea").toggleClass('_hidden');
			});
		},
		submit: function (el, type, code, gap) {
			var form = $(el);
			var name = form.find("input[name='name']").val();
			var phone = form.find("input[name='phone']").val();
			if (name.length != 0 && !/^ *$/.test(name) && phone.length != 0 && !/^ *$/.test(phone)) {
				var data = {
					theme: form.attr("data-theme"),
					/*mail_to: main._default.mail,
					 mail_from: main._default.sender,*/
					name: name,
					phone: phone
				};
				if (type == 'service') {
					yaCounter10660447.reachGoal(code);
				} else if (type == 'mobile_foot-consult') {
					yaCounter10660447.reachGoal('CALL');
				}
			} else {
				var errors = false;
				form.find(".form_line input").each(function () {
					if ($(this).val().length === 0 || /^ *$/.test($(this).val())) {
						$(this).addClass("error");
						$(this).parent().append("<i>Заполните поле правильно</i>");
						errors = true;
					}
				});
				if (errors) {
					return false;
				}
			}

			if (!!gap) {
				if (gap == 'price') {
					ga('send', 'event', 'main', 'presentation');
				} else if (gap == 'callback') {
					ga('send', 'event', 'header', 'callback');
				} else if (gap == 'question') {
					ga('send', 'event', 'all', 'question');
				} else if (gap == 'footer') {
					ga('send', 'event', 'footer', 'callback');
				} else {
					ga('send', 'event', gap, 'request');
				}
			}
		},
		close: function () {
			$("#pop-up").remove();
			$("body").removeClass("pop-up");
		},
		resize: function () {
			$("#pop-up .pop-up-body").each(function () {
				$(this).css({"margin-left": -($(this).width() / 2), "margin-top": -($(this).height() / 2)});
			});
		}
	}
};

var onYouTubePlayerReady = function (playerId) {
	ytplayer = document.getElementById("myytplayer");
};
function parseCookie(name) {
	//   $.cookiePublic.path = '/';
	/*  var matches = document.cookie.match(new RegExp(
	 "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	 ));
	 return matches ? decodeURIComponent(matches[1]) : 'empty';*/
	var val;
	if ($.cookiePublic(name)) {
		val = $.cookiePublic(name);
	} else {
		$.cookiePublic(name, '');
		val = $.cookiePublic(name);
	}
	return val;
}

function InitCookie(name) {
	$.cookiePublic.path = '/';
	var data = parseCookie(name);
	if (!data) return false;

	var items = data.split(',');
	var count = 0;
	for (var i = 0; i < items.length; i++) {
		if (items[i] == '') continue;
		var item = items[i].split(':');
		if (item[1] > 0) {
			count++;
			$('.coctails_catalog-item .btn[data-id = ' + item[0] + ']').addClass('added').text("ЗАКАЗАНО");
			;
		}
	}
	$(".coctails_cart i").each(function () {
		$(this).text("" + count + " " + main.coctails.declens(count, ['коктейль', 'коктейля', 'коктейлей']));
	});

}
function DeleteCookie(name) {
	$.cookiePublic(name, '', {path: '/'});
	return true;
}
function checkFromCookie(name, id) {
	var data = parseCookie(name);
	if (!data) return false;
	var items = data.split(',');
	for (var i = 0; i < items.length; i++) {
		var item = items[i].split(':');
		if (item[0] == id) return item[1];
	}
	return 0;
}
function saveInCookie(name, id, count) {
	$.cookiePublic.path = '/';
	//   var cookieString = name + '=';
	var cookieString = '';
	var change = false;
	var data = $.cookiePublic(name);

	if (data != 'empty') {
		var items = data.split(',');
		for (var i = 0; i < items.length; i++) {
			var item = items[i].split(':');
			if (item[0] == id) {
				if ((typeof item[0] !== "undefined") && (item[0] != '')) {
					cookieString += (id + ':' + count) + ',';
					change = true;
				}
			} else if ((typeof item[0] !== "undefined") && (item[0] != '')) {
				cookieString += (item[0] + ':' + item[1]) + ',';
			}
		}
	}
	if (!change) {
		if ((typeof id !== "undefined") && (id != '')) {
			cookieString += (id + ':' + count) + ',';
		}
	}
	$.cookiePublic(name, cookieString, {path: '/'});
	return true;
}
function deleteFromCookie(name, id) {
	$.cookiePublic.path = '/';
	var data = parseCookie(name);
	console.log(data);
	var cookieString = '';
	if (data) {
		var items = data.split(',');
		for (var i = 0; i < items.length; i++) {
			var item = items[i].split(':');
			if (item[0]) {
				if (item[0] != id) {
					cookieString += (item[0] + ':' + item[1]) + ',';
				}
			}
		}
	} else {
		return false;
	}
//    document.cookie = cookieString + '; expires='+date.toUTCString()+'; path=/';
	$.cookiePublic(name, cookieString, {path: '/'});
	return true;
}
function CheckAnchor(page) {
	var url = window.top.location.pathname;
	var hash = window.top.location.hash;
	var subhash = hash.substring(hash.indexOf("#") + 1);
	if ((url == page) && (subhash != '')) {

		var params = {};
		$(".service-coctails_filter").closest(".service-coctails_filter").find("input[name='bars']").each(function () {
			var key = $(this).attr("name");
			params[key] = $(this).find("option:checked").attr("value");
		});
		$(".service-coctails_filter").closest(".service-coctails_filter").find("input").each(function () {
			if ($(this).is(":checked")) {
				var key = $(this).attr("name");
				params[key] = $(this).attr("value");
			}
		});
		$.ajax({
			url: '/coctails',
			type: 'post',
			data: {
				ajax: 'Y',
				//  alco: params['alco'],
				bars: params['bars'],
				ingredients: params['ingredients'],
				bars_code: subhash
			},
			success: function (data) {
				if (data) {
					$('section.block-cont.g-wrap').html(data);
					/* $('select[name="bars"] option').removeAttr("selected");
					 $('select[name="bars"] option[data-url='+subhash+']').attr('selected','selected');
					 */

					$('input[name="bars"]').removeAttr("checked");
					$('label.bar-filter-label').removeClass("active");
					$('input[name="bars"][data-url=' + subhash + ']').attr('checked', 'checked');
					var id_bar = $('input[name="bars"][data-url=' + subhash + ']').val();
					$('label.bar-filter-label[for="bar' + id_bar + '"]').addClass("active");
					main.init();
					window.top.location.hash = hash;


				}
			},
			error: function (xhr, ajaxOptions, thrownError) {

			}
		});
	}
}

function flyInCart(id) {
	var image = $('#coct' + id),
		copy = image.clone(),
		outer = $('<div>', {class: 'fly'}).appendTo('body');
	outer.html(copy).css({
		top: image.offset().top,
		left: image.offset().left
	}).show().animate({
		left: $('.btn-basket').offset().left + 2,
		top: $('.btn-basket').offset().top - 30,
		width: '42px',
		height: '42px'
	}, 500, function () {
		$(this).animate({paddingTop: '50px'}, 300, function () {
			$(this).remove();
		});
	});
}
function checkMail() {
	var email = $('#subscr-email').val();
	var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
	if ((email == "") || (!pattern.test(email))) {
		$('#subscr-email').addClass('error');
		return false;
	} else {
		$("#Send").val(2);
		ga('send', 'event', 'main', 'mail');
		return true;
	}
}
(function ($) {
	$.fn.selectbox = function () {
		$(this).each(function () {
			var select = $(this);
			if (select.prev('span.selectbox').length < 1) {
				function doSelect() {
					var option = select.find('option');
					var optionSelected = option.filter(':selected');
					var optionText = option.filter(':first').text();
					if (optionSelected.length) optionText = optionSelected.text();
					var ddlist = '';
					for (i = 0; i < option.length; i++) {
						var selected = '';
						var disabled = ' class="disabled"';
						if (option.eq(i).is(':selected')) selected = ' class="selected sel"';
						if (option.eq(i).is(':disabled')) selected = disabled;
						ddlist += '<li' + selected + '>' + option.eq(i).text() + '</li>';
					}
					var selectbox = $('<span class="selectbox" style="display:inline-block;position:relative">' + '<div class="select" style="float:left;position:relative;z-index:10000"><div class="text">' + optionText + '</div>' + '<b class="trigger"><i class="arrow"></i></b>' + '</div>' + '<div class="dropdown" style="position:absolute;z-index:9999;overflow:auto;overflow-x:hidden;list-style:none">' + '<ul>' + ddlist + '</ul>' + '</div>' + '</span>');
					select.before(selectbox).css({position: 'absolute', top: -9999});
					var divSelect = selectbox.find('div.select');
					var divText = selectbox.find('div.text');
					var dropdown = selectbox.find('div.dropdown');
					var li = dropdown.find('li');
					var selectHeight = selectbox.outerHeight();
					if (dropdown.css('left') == 'auto') dropdown.css({left: 0});
					if (dropdown.css('top') == 'auto') dropdown.css({top: selectHeight});
					var liHeight = li.outerHeight();
					var position = dropdown.css('top');
					dropdown.hide();
					divSelect.click(function () {
						var topOffset = selectbox.offset().top;
						var bottomOffset = $(window).height() - selectHeight - (topOffset - $(window).scrollTop());
						if (bottomOffset < 0 || bottomOffset < liHeight * 6) {
							dropdown.height('auto').css({top: 'auto', bottom: position});
							if (dropdown.outerHeight() > topOffset - $(window).scrollTop() - 20) {
								dropdown.height(Math.floor((topOffset - $(window).scrollTop() - 20) / liHeight) * liHeight);
							}
						} else if (bottomOffset > liHeight * 6) {
							dropdown.height('auto').css({bottom: 'auto', top: position});
							if (dropdown.outerHeight() > bottomOffset - 20) {
								dropdown.height(Math.floor((bottomOffset - 20) / liHeight) * liHeight);
							}
						}
						$('span.selectbox').css({zIndex: 1}).removeClass('focused');
						selectbox.css({zIndex: 2});
						if (dropdown.is(':hidden')) {
							$('div.dropdown:visible').hide();
							dropdown.show();
							selectbox.addClass('infocus');
						} else {
							dropdown.hide();
							selectbox.removeClass('infocus');
						}
						return false;
					});
					li.hover(function () {
						$(this).siblings().removeClass('selected');
					});
					var selectedText = li.filter('.selected').text();
					li.filter(':not(.disabled)').click(function () {
						var liText = $(this).text();
						if (selectedText != liText) {
							$(this).addClass('selected sel').siblings().removeClass('selected sel');
							option.removeAttr('selected').eq($(this).index()).attr('selected', true);
							selectedText = liText;
							divText.text(liText);
							select.change();
						}
						dropdown.hide();
						selectbox.removeClass('infocus');
					});
					dropdown.mouseout(function () {
						dropdown.find('li.sel').addClass('selected');
					});
					select.focus(function () {
						$('span.selectbox').removeClass('focused');
						selectbox.addClass('focused');
					}).keyup(function () {
						divText.text(option.filter(':selected').text());
						li.removeClass('selected sel').eq(option.filter(':selected').index()).addClass('selected sel');
					});
					$(document).on('click', function (e) {
						if (!$(e.target).parents().hasClass('selectbox')) {
							dropdown.hide().find('li.sel').addClass('selected');
							selectbox.removeClass('focused').removeClass('infocus');
						}
					});
				}

				doSelect();
				select.on('refresh', function () {
					select.prev().remove();
					doSelect();
				})
			}
		});
	}
})(jQuery);

Share = {
	vk: function (purl, ptitle, pimg, text) {
		var url = 'http://vkontakte.ru/share.php?';
		url += 'url=' + encodeURIComponent(purl);
		url += '&title=' + encodeURIComponent(ptitle);
		url += '&description=' + encodeURIComponent(text);
		url += '&image=' + encodeURIComponent(pimg);
		url += '&noparse=true';
		Share.open(url);
	},
	facebook: function (purl, ptitle, pimg, text) {
		var url = 'http://www.facebook.com/sharer.php?s=100';
		url += '&p[title]=' + encodeURIComponent(ptitle);
		url += '&p[summary]=' + encodeURIComponent(text);
		url += '&p[url]=' + encodeURIComponent(purl);
		url += '&p[images][0]=' + encodeURIComponent(pimg);
		Share.open(url);
	},
	twitter: function (purl, ptitle) {
		var url = 'http://twitter.com/share?';
		url += 'text=' + encodeURIComponent(ptitle);
		url += '&url=' + encodeURIComponent(purl);
		url += '&counturl=' + encodeURIComponent(purl);
		Share.open(url);
	},
	open: function (url) {
		window.open(url, '_blank');
	}
};

$(function () {
	$(".js-fancybox").fancybox();

	// slider
	var $sliderMain = $('.js-slider-main'),
		$video = $sliderMain.find('.js-video'),
		$btnVideo = $sliderMain.find('.js-btn-video'),
		$info = $sliderMain.find('.js-slider-title, .js-slider-description');

	function videoPause() {
		$video[0].pause();
	}

	$sliderMain.bxSlider({
		mode: 'fade',
		captions: true,
		onSlideAfter: function () {
			videoPause();
			$btnVideo.removeClass('active');
			$info.removeClass('hide');
		},
		onSlideBefore: function () {
			videoPause();
			$btnVideo.removeClass('active');
			$info.removeClass('hide');
		}
	});
	$btnVideo.on('click', function () {
		var $this = $(this),
			$thisVideo = $this.closest('.js-slider-item').find('.js-video');
		$thisVideo.fadeIn(500);
		if ($this.hasClass('active')) {
			$this.removeClass('active');
			$thisVideo[0].pause();
			$info.removeClass('hide');
		} else {
			$this.addClass('active');
			$thisVideo[0].play();
			$info.addClass('hide');

		}
	});

	// feedShare
	var $shareBtn = $('.js-feed-share');
	$shareBtn.on('click', function () {
		var $this = $(this);
		if ($this.hasClass('active')) {
			$this.removeClass('active').find('.js-feed-share-dropdown').fadeOut(100);
		} else {
			$this.addClass('active').find('.js-feed-share-dropdown').fadeIn(100);
		}
	});

	$(".js-feed-list-more").on('click', function (e) {
		e.preventDefault();

		var shown = 0, toShow = $(this).data('to-show');
		$(".feed-list__item.hidden").each(function () {
			if (shown++ < toShow) {
				$(this).removeClass('hidden');
			}
		});

		if (!$(".feed-list__item.hidden").length) {
			$(this).remove();
		}
	});

	$(".js-share").on('click', function (e) {
		e.preventDefault();

		Share[$(this).data('target')]($(this).data('url'), $(this).data('title'), $(this).data('img'), $(this).data('text'));
	});

	$(".service__other-tabs a").on("click", function(e){
		e.preventDefault();

		var el = $(this);
		var id = el.attr("data-tab");
		if(!el.hasClass("active")){
			//header
			var tabs_header = el.closest(".tabs__header");
			tabs_header.find("a.active").removeClass("active");
			el.addClass("active");

			//content
			var tabs_content = tabs_header.next();
			tabs_content.find(".tab.active").removeClass("active");
			tabs_content.find(".tab[data-tab='"+id+"']").addClass("active");
		}
	});
});